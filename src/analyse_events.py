from itertools import combinations
from ete3 import Tree, TreeStyle, NodeStyle
import matplotlib.pyplot as plt
import pymongo
from urllib.parse import urlparse

import analysis_utils
import db_utils


class Event():
  def __init__(self, articles):
    self.articles = articles

class Article():
  def __init__(self, id, url, title, body, author, date, keywords):
    self.id = id
    self.url = url
    self.title = title
    self.body = body
    self.author = author
    self.date = date
    self.keywords = keywords

  def __repr__(self):
    # return repr(urlparse(self.url).hostname)
    return repr(self.id)

  def __str__(self):
    return 'ID: {0}\n'\
           'Title: {1}\n'\
           'Date: {2}\n'\
           'Url: {3}\n'.format(self.id, self.title, self.date, urlparse(self.url).hostname)


def add_similarity(cmbs):
  for c in cmbs:
    yield [c[0], c[1], analysis_utils.texts_similarity(c[0].body, c[1].body)]


def get_similarity_combinations(articles):
  cmbs = list(combinations(articles, 2))
  scores = [c for c in add_similarity(cmbs)]
  return sorted(scores, key=lambda x: x[2], reverse=True)


def get_db_articles(event, database):
  for i,item in enumerate(event['articles'], start=1):
    article = database['news'].find({'url': item['url']})[0]
    yield Article(id=i,
                  url=article['url'],
                  title=article['title'],
                  body=article['body'],
                  author=article['author'],
                  date=article['date'],
                  keywords=article['keywords'])


def get_db_events(database):
  for doc in database['events'].find({}):
    yield Event([article for article in get_db_articles(doc, database)])


def get_events():
  database = db_utils.database()
  return [event for event in get_db_events(database)]


def add_leaf(tree_str, new_leaf):
  return '({0},{1})'.format(tree_str[:-1], new_leaf)


def set_to_str(a):
  res = ''
  for elem in a:
    if res:
      res = '{},{}'.format(res, repr(elem))
    else:
      res = repr(elem)
  return '({})'.format(res)


def get_tree_str(scores):
  res = ''
  used = set()
  for x in [1, 0.9, 0.8, 0.7, 0.6, 0.4, 0.2, 0.1]:
    tmp = set()
    for s in scores:
      if s[2] <= x and s[2] > (x-0.1):
        if s[0] not in used:
          tmp.add(s[0])
        if s[1] not in used:
          tmp.add(s[1])
    if tmp:
      if res:
        res = '({},{})'.format(res, set_to_str(tmp))
      else:
        res = set_to_str(tmp)
      used = used.union(tmp)
  return res


def draw_tree(string_repr):
  t = Tree('({});'.format(string_repr))
  ts = TreeStyle()
  ts.show_leaf_name = True

  nstyle = NodeStyle()
  nstyle["shape"] = "sphere"
  nstyle["size"] = 10
  nstyle["fgcolor"] = "darkred"

  for n in t.traverse():
    if not n.is_leaf():
      n.set_style(nstyle)

  ts.branch_vertical_margin = 10
  t.show(tree_style=ts)


def main():
  events = [event for event in get_events()]
  for article in events[0].articles:
    print(article)
  sorted_scores = get_similarity_combinations(events[0].articles)
  string_repr = get_tree_str(sorted_scores)
  # string_repr = '(((a,b),c),d,e);'
  draw_tree(string_repr)

if __name__ == "__main__":
  main()