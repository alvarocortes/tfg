import pymongo

database_host = 'localhost'
database_port = 27017
database_name = 'tfg'


def mongo_client():
  return pymongo.MongoClient(host=database_host, port=database_port)


def database():
  client = mongo_client()
  return client[database_name]


def reset_db():
  client = mongo_client()
  client.drop_database(database_name)
