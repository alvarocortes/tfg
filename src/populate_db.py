from tqdm import tqdm as show_progress_bar
from datetime import datetime
from time import mktime
import feedparser
import newspaper
import db_utils
import pymongo
import os


rss_links_filepath = os.path.join('.', 'src', 'data', 'rss_links.txt')
# rss_links_filepath = os.path.join('.', 'src', 'data', 'test.txt')
mongodb_lang = 'spanish'
newspaper_lang = 'es'

database = db_utils.database()


def get_article(article_url):
  try:
    article = newspaper.Article(article_url, language=newspaper_lang)
    article.download()
    article.parse()
    article.nlp()
    return article
  except Exception:
    pass


def get_document(rss_item):
  article = get_article(rss_item['url'])
  if article:
    return {
      'url': article.url,
      'title': article.title,
      'body': article.text,
      'author': next(iter([rss_item['author'], article.authors]), None),
      'date': next(iter([rss_item['pubdate'], article.publish_date]), None),
      'keywords': ' '.join(article.keywords)
    }


def insert_in_db(rss_item):
  document = get_document(rss_item)
  if document:
    database['news'].insert_one(document)


def get_rss_items(rss_links_file):
  with open(rss_links_file, 'r') as f:
    for rss_feed_link in f.read().splitlines():
      fp = feedparser.parse(rss_feed_link)
      for entry in fp.entries:
        article_url = entry.get('link', None)
        article_author = entry.get('author', None)
        article_pubdate = entry.get('published_parsed', None)
        if article_pubdate:
          article_pubdate = datetime.fromtimestamp(mktime(article_pubdate))
        if article_url:
          yield {'url': article_url,
                 'author': article_author,
                 'pubdate': article_pubdate}


def set_db_text_index():
  database['news'].create_index(keys=[('title', pymongo.TEXT),
                                      ('body', pymongo.TEXT),
                                      ('keywords', pymongo.TEXT)],
                                weights={'title': 10,
                                        'body': 1,
                                        'keywords': 1},
                                name='custom_index',
                                default_language=mongodb_lang)


def main():
  db_utils.reset_db()
  db_urls = set()
  set_db_text_index()
  rss_items = [i for i in get_rss_items(rss_links_filepath)]
  for rss_item in show_progress_bar(rss_items):
    if rss_item['url'] not in db_urls:
      db_urls.add(rss_item['url'])
      insert_in_db(rss_item)


if __name__ == "__main__":
  main()
  
  