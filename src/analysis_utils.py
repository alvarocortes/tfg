from sumy.evaluation import rouge_2
import textdistance


def texts_similarity(text1, text2):
  return textdistance.lzma_ncd.normalized_similarity(text1, text2)


def rouge_summaries(summary1, summary2):
  return rouge_2(summary1, summary2)


def main():

  text1 = 'John es el mejor'
  text2 = 'John es el capitan'

  print(texts_similarity(text1, text2))
  


if __name__ == "__main__":
  main()
