from tqdm import tqdm as show_progress_bar
import collections
import pymongo
import spacy
spacy_nlp = spacy.load('es_core_news_sm')

import db_utils
database = db_utils.database()

processed_articles = set()


def get_all_articles():
  for doc in database['news'].find({}):
    yield doc


def get_total_number_of_articles():
  return database['news'].count_documents({})


def find_docs(search_text):
  res = database['news'].find({"$text": {"$search": search_text}},
                                     {'score': {'$meta': 'textScore'}})
  for record in res.sort([('score', {'$meta': 'textScore'})]):
    yield record


def contains_special_char(string):
  special_chars = ',."\';:/“”»«<>*()=+^%$'
  return any(char in string for char in special_chars)


def get_proper_nouns(text, max_count=6):
  pos_tagged = spacy_nlp(text)
  proper_nouns = [w.text for w in pos_tagged if w.pos_ == 'PROPN']
  proper_nouns = list(filter(lambda x: not contains_special_char(x), proper_nouns))
  counter = collections.Counter(proper_nouns)
  return [w[0] for w in counter.most_common(max_count)]


def get_lists_intersection(list_a, list_b):
  return set(list_a).intersection(list_b)


def get_search_string(proper_nouns, keywords):
  return '{0} {1}'.format(' '.join(proper_nouns), keywords)


def get_list_of_related_articles(document):
  processed_articles.add(document['url'])

  # Prepare the search_text
  pnouns = get_proper_nouns(document['body'])
  search_text = get_search_string(pnouns, document['keywords'])

  list_of_related_news = [{'url': document['url']}]

  stop_looking = 0
  iterdocs = iter(find_docs(search_text))
  next(iterdocs)
  for doc in iterdocs:
    np_list_tmp = get_proper_nouns(doc['body'])
    common_pn = get_lists_intersection(np_list_tmp, pnouns)

    if doc['url'] not in processed_articles and len(common_pn) >= 4:
      list_of_related_news.append({'url': doc['url']})
      processed_articles.add(doc['url'])
    else:
      stop_looking = stop_looking + 1
    
    if stop_looking == 3:
      break

  return list_of_related_news


def get_list_of_events():
  n_art = get_total_number_of_articles()
  for article in show_progress_bar(get_all_articles(), total=n_art):
    if article['url'] not in processed_articles:
      yield get_list_of_related_articles(article)
    

def get_top_trending_events(list_of_lists, limit):
  return sorted(list_of_lists, key=len, reverse=True)[:limit]


def insert_in_db_top_trending_events(events, limit=10):
  for list_of_articles in get_top_trending_events(events, limit):
    database['events'].insert_one({'articles': list_of_articles})


def main():
  events = [event for event in get_list_of_events()]
  insert_in_db_top_trending_events(events)


if __name__ == "__main__":
  main()

  
  