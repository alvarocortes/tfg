# Installation
install:
	python -m pip install -r requirements.txt
	python -m spacy download es_core_news_sm
	python -m spacy download es_core_news_md

# MongoDB
start_db:
	mongod --dbpath ./db_data/ --port 27017

setup_db:
	python ./src/populate_db.py
	python ./src/group_by_event.py

get_most_popular_event:
	python ./src/analyse_events.py

run:
	install
	setup_db
	get_most_popular_event


.PHONY: install
